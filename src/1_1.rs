use std::io::{self, BufRead};
use std::str::FromStr;

fn main() {
    let stdin = io::stdin();
    let frequencies = stdin.lock().lines().map(|line_result| {
        i32::from_str(&line_result.unwrap()).unwrap()
    });
    let total_drift = frequencies.fold(0, |drift, frequency| { drift + frequency });
    println!("{}", total_drift)
}
